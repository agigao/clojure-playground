(ns playground.core-test
  (:require [clojure.test :refer :all]
            [playground.core :refer :all]))

(deftest sator-rotas-is-palindrome
    (is (= (is-palindrome? "Sator, Arepo, Tenet, Opera, Rotas") true)))

(deftest those-are-anagrams
  (is (= (are-anagrams? "William Shakespeare" "I am a weakish speller") true)))

(deftest intersection-v
  (is (= (find-commons-v [ "Clj" 1 2 3] [2 3 10 5 10 214512 "C" 12512 25151 "Clj"]) ["Clj" 2 3])))

(deftest intersection-s
  (is (= (find-commons-s [1 2 3 "Good-Morning"] [2 3 10 11 12 "Good-Morning"]) #{3 2 "Good-Morning"})))

(deftest number-to-binary-8-2r1000
  (is (= (number-to-binary 8) "1000")))

(deftest negative-num-to-binary
  (is (re-find #"shhh" (number-to-binary -8))))
