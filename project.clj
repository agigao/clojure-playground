(defproject playground "0.1.0-SNAPSHOT"
  :description "Playground: toy functions in Clojure"
  :url "http://src.chokheli.rocks/clojure-playground"
  :license {:name "GPLv3"
            :url "https://gnu.org/licenses/gpl.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :main playground.core)