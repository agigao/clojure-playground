(ns playground.core
  "playground for Clojure"
  (:require [clojure.string :as string]
            [clojure.set :as set]))

; - Write code to check a String is palindrome or not.
(defn is-palindrome?
  "Check whether the provided string is palindrome"
  [palindrome]
  (let [text (string/lower-case
              (string/replace palindrome " " ""))]
    (= text (string/reverse text))))

(is-palindrome? "Sator, Arepo, Tenet, Opera, Rotas")
;; => true

; - Determine if 2 Strings are anagrams
(defn reprocess
  "The utility macro for reprocessing anagrams down to 'raw' material"
  [text]
  (->>
   (string/replace text #"[^=A-Za-z]*" "")
   (string/lower-case)
   (seq)
   (sort)
   (string/join)))
;; => aaaeeehiikllmprssw


(defn are-anagrams?
  "The actual function that returns boolean value according to '=' conditional operator."
  [text-one text-two]
  (let [reprocessed-one (reprocess text-one)
        reprocessed-two (reprocess text-two)]
    (= reprocessed-one reprocessed-two)))

(are-anagrams? "William Shakespeare" "I am a weakish speller")
;; => true

; - Find the common elements of 2 "int" array
; The first - manual version
(defn find-commons-v
  "Manual, tail-recursive function, for collecting common elements of two vectors"
  ([coll-one coll-two] (find-commons-v coll-one coll-two []))
  ([coll-one coll-two commons]
   (if (or (empty? coll-two) (empty? coll-one)) commons
       (if (some #(= (first coll-one) %) coll-two)
         (recur (rest coll-one) coll-two (conj commons (first coll-one)))
         (recur (rest coll-one) coll-two commons)))))

(find-commons-v ["Clj" 1 2 3] [2 3 10 5 10 214512 "C" 12512 25151 "Clj"])
;; => ["Clj" 2 3]

; The second - shortcut
(defn find-commons-s
  "Clojure way of intersection.
  Here we have one crucial difference in contrast of previous function, I mean 'find-commons' -
  coll-intersection returns a set - sets doesn't allow duplicate values in collection
   whereas find-commons returns vector and vectors doesn't care about duplicity."
  [coll-one coll-two]
  (set/intersection (set coll-one) (set coll-two)))

(find-commons-s [1 2 3 "Good-Morning"] [2 3 10 11 12 "Good-Morning"])
;; => #{3 2 "Good-Morning"}


; - Write a function that prints out the binary form of an int
(defn number-to-binary
  "takes number and returns string of binary representation (of unsigned ones)"
  ([number] (number-to-binary number ""))
  ([number binary]
   (if (or (neg? number) (float? number))
     "Do it yourself. I'm on a vacation. shhh, don't use java library."
     (if (zero? number)
       binary
       (recur (bigint (/ number 2))
              (str (mod number 2) binary))))))

(number-to-binary 76342737425724)
;; => "10001010110111011101101101111101101010100111100"
