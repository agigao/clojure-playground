# Playground in Clojure

I've playing with my brain-cells for a while, thus - an outcome.

## Usage Guide for macOS:

- brew install leiningen
- git clone http://src.chokhe.li/clojure-playground.git
- cd clojure-playground
- lein test

## Clojure REPL Presentation in IntelliJ (4K)
- http://www.dailymotion.com/video/x498lwr

## License

Copyright © 2016 Giga Chokheli